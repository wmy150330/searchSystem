import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/pages/home/Home'
import Card from '@/pages/card/Card'
import LoginCommon from '@/pages/login/login-common/LoginCommon'
import LoginCard from '@/pages/login/login-card/LoginCard'
import LoginClass from '@/pages/login/login-class/LoginClass'
import ServiceCommon from '@/pages/service-common/ServiceCommon'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/login-common',
      name: 'LoginCommon',
      component: LoginCommon
    },
    {
      path: '/login-card',
      name: 'LoginCard',
      component: LoginCard
    },
    {
      path: '/login-class',
      name: 'LoginClass',
      component: LoginClass
    },
    {
      path: '/service-common',
      name: 'ServiceCommon',
      component: ServiceCommon
    },
    {
      path: '/card/:xm',
      name: 'Card',
      meta: {
        requireAuth: true
      },
      component: Card
    }
  ]
})
